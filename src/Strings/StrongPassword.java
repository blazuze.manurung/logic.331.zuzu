package Strings;

import java.util.Scanner;

public class StrongPassword {
    private static Scanner input = new Scanner(System.in);
    private static String specialChar = "!@#$%^&*()-+";
    public static void Resolve() {
        boolean isUpperCase = true;
        boolean isLowerCase = true;
        boolean isDigit = true;
        boolean isSimbol = true;

        int hitungSyarat = 0;
        int harusMasukkan = 0;

        System.out.println("===Strong Password===");
        System.out.println("Syara - Syarat :");
        System.out.println("1. Terdiri dari minimal 6 Character");
        System.out.println("2. Memiliki minimal 1 angka");
        System.out.println("3. Memiliki minimal 1 EnglishLowerCase");
        System.out.println("4. Memiliki minimal 1 EnglishUpperCase");
        System.out.println("5. Memiliki minimal 1 Special Character: !@#$%^&*()-+");
        System.out.println("Masukkan Password : ");
        String text = input.nextLine();

        char[] huruf = text.toCharArray();
        char[] simbol = specialChar.toCharArray();

        if (huruf.length >= 6) {
            hitungSyarat++;
            for (int i = 0; i < huruf.length; i++) {
                if (Character.isUpperCase(huruf[i]) && isUpperCase) {
                    isUpperCase = false;
                    hitungSyarat++;
                } else if (Character.isLowerCase(huruf[i]) && isLowerCase) {
                    isLowerCase = false;
                    hitungSyarat++;
                } else if (Character.isDigit(huruf[i]) && isDigit) {
                    isDigit = false;
                    hitungSyarat++;
                } else {
                    for (int j = 0; j < simbol.length; j++) {
                        if (huruf[i] == simbol[j] && isSimbol) {
                            hitungSyarat++;
                            isSimbol = false;
                            break;
                        }
                    }
                }
            }
            if (isUpperCase) {
                System.out.println("Tidak ada Huruf Besar");
            }

            if (isLowerCase) {
                System.out.println("Tidak ada Huruf Kecil");
            }

            if (isDigit) {
                System.out.println("Tidak ada Angka");
            }

            if (isSimbol) {
                System.out.println("Tidak ada Simbol");
            }
            if (hitungSyarat == 5)
                System.out.println("Password is Strong");

        } else {
            harusMasukkan = 6 - huruf.length;
            System.out.println("Password Kurang dari 6 Character");
            System.out.println("Masukkan " + harusMasukkan + " Character Lagi");
        }
    }
}