package Strings;

import javax.imageio.metadata.IIOMetadataFormatImpl;
import java.sql.SQLOutput;
import java.util.HashSet;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Gemstones {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Input Banyak Data : ");
        int banyakData = input.nextInt();

        String[] data = new String[banyakData];

        input.nextLine();
        for (int i = 0; i < banyakData; i++) {
            System.out.println("Masukkan Data " + (i+1) + " : ");
            data[i] = input.nextLine();
        }
        String stringPertama = data[0];

        HashSet<String>setStringPertama = new HashSet<String>();

        for (int i = 0; i < stringPertama.length(); i++) {
            setStringPertama.add(stringPertama.substring(i, i+1));
        }

        boolean exist = true;
        int total = 0;

        for (String i: setStringPertama) {
            for (int j = 1; j < data.length ; j++) {
                if (!data[j].contains(i))
                {
                    exist = false;
                    break;
                }
            }

            if (exist)
            {
                total++;
            }
            else
            {
                exist = true;
            }
        }

        System.out.println(total);
    }
}
