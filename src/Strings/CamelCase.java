package Strings;

import java.util.Scanner;

public class CamelCase {
    private static Scanner input = new Scanner(System.in);
    public  static void Resolve()
    {
        System.out.println("===Hitung Jumlah Kata dari CamelCase===");
        System.out.println("Contoh : saveChangesInTheEditor");
        System.out.println("Jumlah Kata : 5");
        String text = input.nextLine();
        int hitung = 1;

        char[] huruf = text.toCharArray();

        for (int i = 0; i < huruf.length; i++) {
            if(Character.isUpperCase(huruf[i]))
                hitung++;
        }
        System.out.println("Jumlah Kata : " + hitung);
    }
}
