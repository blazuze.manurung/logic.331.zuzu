package Strings;

import java.util.Scanner;

public class CaesarChipher {
    private static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("===Caesar Cipher===");
        System.out.println("Masukkan data untuk di rubah: ");
        String text = input.nextLine();
        char[] huruf = text.toCharArray();

        System.out.println("Range jauh perubahannya : ");
        int hitung = input.nextInt();

        if(hitung > 26)
        {
            hitung = hitung % 26;
        }

        for (int i = 0; i < huruf.length; i++) {
            if (huruf[i] >= 'a' && huruf[i] <= 'z')
            {
                huruf[i] += hitung;
                if(huruf[i] > 'z')
                {
                    huruf[i] -= 26;
                }
            }
            else if (huruf[i] >= 'A' && huruf[i] <= 'Z')
            {
                huruf[i] += hitung;
                if(huruf[i] > 'Z')
                {
                    huruf[i] -= 26;
                }
            }
            System.out.print(huruf[i]);
        }
    }
}

