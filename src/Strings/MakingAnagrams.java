package Strings;

import java.util.Scanner;

public class MakingAnagrams {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("===Making Anagrams===");
        System.out.println("Masukkan Data Pertama : ");
        String dataPertama = input.nextLine().toLowerCase();
        System.out.println("Masukkan Data Kedua : ");
        String dataKedua = input.nextLine().toLowerCase();

        int hitung = 0;
        int banyakDataTidakSama = 0;
        int semuaData = dataPertama.length() + dataKedua.length();

        for (int i = 0; i < dataPertama.length(); i++) {
            if (dataKedua.contains(dataPertama.substring(i,i + 1)))
            {
                hitung++;
            }
        }

        banyakDataTidakSama = semuaData - (hitung * 2);

        System.out.println(banyakDataTidakSama);

    }
}
