package Strings;

public class Utility {
    public static char[] ConvertStringToArraychar (String text){

        String[] textArray = text.split(" ");
        char[] charArray = new char[textArray.length];

        for (int i = 0; i < textArray.length; i++)
        {
            charArray[i] = textArray[i].charAt(0);
        }

        return charArray;
    }
}

