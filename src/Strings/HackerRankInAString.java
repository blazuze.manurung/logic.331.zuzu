package Strings;

import WarmUp.Utility;

import java.util.Scanner;

public class HackerRankInAString {
    public static Scanner input = new Scanner(System.in);
    private static String sama = "hackerrank";
    public static void Resolve()
    {
        System.out.println("===HackerRank in a String===");
        System.out.println("Membandingkan data yang masuk, apa data tersebut memiliki tulisan : hackerrank ");
        System.out.println("Masukkan Data : ");
        String data = input.nextLine();
        char[] huruf = data.toCharArray();
        char[] harusSama = sama.toCharArray();
        int hitungSama = 0;
//        boolean udahTerhitung = true;

//        for (int i = 0; i < harusSama.length; i++) {
//            if(i == 0)
//            {
//                for (int j = 0; j < huruf.length; j++) {
//                    if (huruf[j] == harusSama[i] && udahTerhitung)
//                    {
//                        hitungSama++;
//                        udahTerhitung = false;
//                    }
//                }
//            }
//            else
//            {
//                for (int j = 0; j < i; j++) {
//                    if(huruf[j] != harusSama[i])
//                    {
//                        udahTerhitung = true;
//                    }
//                }
//                if (udahTerhitung)
//                {
//                    for (int j = 0; j < huruf.length; j++) {
//                        if(huruf[j] == harusSama[i] && udahTerhitung)
//                        {
//                            hitungSama++;
//                            udahTerhitung = false;
//                        }
//                    }
//                }
//            }
        for (int i = 0; i < huruf.length; i++) {
            if (huruf[i] == harusSama[hitungSama])
            {
                hitungSama++;
            }
        }
        if (hitungSama >= harusSama.length)
        {
            System.out.println("Dari data yang dimasukkan TERDAPAT tulisan: hackerrank");
        }
       else
        {
            System.out.println("Dari data yang dimasukkan TIDAK TERDAPAT tulisan : hackerrank");
        }
    }
}
