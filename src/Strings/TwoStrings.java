package Strings;

import java.util.Scanner;

public class TwoStrings {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("===Two Strings===");
        System.out.println("Membandingkan 2 Data Apakah dalam Kedua Data Memiliki Kesamaan");
        System.out.println("Masukkan Data Pertama : ");
        String dataSatu = input.nextLine();
        System.out.println("Masukkan Data Kedua : ");
        String dataDua = input.nextLine();

        int hitung = 0;

        char[] dataCharSatu = dataSatu.toCharArray();
        char[] dataCharDua= dataDua.toCharArray();

        for (int i = 0; i < dataCharSatu.length; i++) {
            for (int j = 0; j < dataCharDua.length; j++) {
                if(dataCharSatu[i] == dataCharDua[j])
                {
                    hitung++;
                }
            }
        }

        if(hitung > 0)
        {
            System.out.println("Data Kedua ADA Kesamaan Karakter dengan Data Satu");
        }

        else
        {
            System.out.println("Data Kedua TIDAK ADA Kesamaan Karakter dengan Data Satu");
        }
    }
}

