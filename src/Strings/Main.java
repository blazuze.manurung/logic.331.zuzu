package Strings;


import InputPlural.*;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        System.out.println("Pilih Nomor Soal (1-10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10)
        {
            System.out.println("Nomor Soal Tidak Tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1: CamelCase.Resolve();
                break;
            case 2: StrongPassword.Resolve();
                break;
            case 3: CaesarChipher.Resolve();
                break;
//            case 4: Soal04.Resolve();
//                break;
            case 5: HackerRankInAString.Resolve();
                break;
            case 6: Pangrams.Resolve();
                break;
//            case 7: Soal07.Resolve();
//                break;
            case 8: Gemstones.Resolve();
                break;
            case 9: MakingAnagrams.Resolve();
                break;
            case 10: TwoStrings.Resolve();
                break;
        }
    }
}