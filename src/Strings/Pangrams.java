package Strings;

import java.util.Arrays;
import java.util.Scanner;

public class Pangrams {
    public static Scanner input =  new Scanner(System.in);
    private static String alphabet = "qwertyuiopasdfghjklzxcvbnm";
    public static void Resolve()
    {
//        String[] stringAlphabetArray = alphabet.split(",");
        char[] charAlphabetArray = alphabet.toCharArray();
        System.out.println("===Pangrams===");
        System.out.println("Masukkan text : ");
        String text = input.nextLine().toLowerCase();

        int hitung = 0;

//        System.out.println(alphabet.substring(0, 1));

        for (int i = 0; i < charAlphabetArray.length; i++) {
            if (text.contains(alphabet.substring(i, i + 1)))
            {
                hitung++;
            }
        }

        if(hitung == 26)
        {
            System.out.println("Pangrams");
        }
        else
        {
            System.out.println("Bukan Pangrams");
        }

    }
}
