package ProblemSolving14;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        String tanggalPemesanan, liburNasional, hariDikirim, hariSampai;
        int tanggalDikirim, tanggalSampai, countDay = 7, numberDay = 0, indexHari = 0;
        String[] hariSemingggu = {"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"};

        System.out.print("Tanggal dan Hari Pemesanan: ");
        tanggalPemesanan = input.nextLine();

        System.out.print("Hari libur Nasional: ");
        liburNasional = input.nextLine();

        String[] splitTanggalPemesanan = tanggalPemesanan.split(" ");
        String[] strArr = liburNasional.split(", ");
        int[] splitsTanggalLiburNasional = new int[strArr.length];

        for (int i = 0; i < strArr.length; i++) {
            splitsTanggalLiburNasional[i] = Integer.parseInt(strArr[i]);
        }

        tanggalDikirim = Integer.parseInt(splitTanggalPemesanan[0]);
        hariDikirim = splitTanggalPemesanan[1];

        if (hariDikirim.equalsIgnoreCase("Senin")) {
            numberDay = 0;
        }
        else if (hariDikirim.equalsIgnoreCase("Selasa")) {
            numberDay = 1;
        }
        else if (hariDikirim.equalsIgnoreCase("Rabu")) {
            numberDay = 2;
        }
        else if (hariDikirim.equalsIgnoreCase("Kamis")) {
            numberDay = 3;
        }
        else if (hariDikirim.equalsIgnoreCase("Jumat")) {
            numberDay = 4;
        }
        else if (hariDikirim.equalsIgnoreCase("Sabtu")) {
            numberDay = 5;
        }
        else if (hariDikirim.equalsIgnoreCase("Minggu")) {
            numberDay = 6;
        }
        else {
            System.out.println("Hari tidak tersedia!!!");
        }

        while (countDay > 0){
            for (int i = 0; i < splitsTanggalLiburNasional.length; i++) {
                if (tanggalDikirim == splitsTanggalLiburNasional[i]) {
                    tanggalDikirim++;
                    indexHari++;
                    break;
                }
                else {
                    indexHari++;
                    tanggalDikirim++;
                    countDay--;
                }
            }
        }

        for (int i = 0; i < indexHari; i++) {
            numberDay++;

            if (numberDay > 6) {
                numberDay = 0;
            }
        }

        if (tanggalDikirim > 31) {
            tanggalSampai = tanggalDikirim - 31;

            if (numberDay == 5) {
                numberDay = 0;
                tanggalSampai += 2;
            }
            else if (numberDay == 6) {
                numberDay = 0;
                tanggalSampai++;
            }

            hariSampai = hariSemingggu[numberDay];

            System.out.println("Paket sampai pada tanggal " + tanggalSampai + " hari " + hariSampai + " di bulan berikutnya");
        }
        else {
            tanggalSampai = tanggalDikirim;

            if (numberDay == 5) {
                numberDay = 0;
                tanggalSampai += 2;
            }
            else if (numberDay == 6) {
                numberDay = 0;
                tanggalSampai++;
            }

            hariSampai = hariSemingggu[numberDay];

            System.out.println("Paket sampai tanggal " + tanggalSampai + " hari " + hariSampai);
        }
    }

}
