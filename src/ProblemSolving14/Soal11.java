package ProblemSolving14;

import java.util.Scanner;

public class Soal11 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("===Beli Pulsa===");
        System.out.println("0 - 10.000 -> 0 point");
        System.out.println("10.001 - 30.000 -> 1 point setiap kelipatan 1.000");
        System.out.println("> 30.000 -> 2 point setiap kelipatan 1.000");

        System.out.println("Masukkan Nominal Pulsa : ");
        System.out.print("Beli Pulsa : ");
        int pulsa = input.nextInt();
        int point = 0;
        int pointTotal = 0;

        if (pulsa > 30000) {
            point = (pulsa - 30000) / 500;
            pointTotal = 20 + point;
            System.out.println("0 + 20 + " + point + " = " + pointTotal + " Point");
        }

        else if (pulsa > 10000 && pulsa < 30000)
        {
            pointTotal = (pulsa - 10000) / 1000;
            System.out.println("0 + " + pointTotal + " = " + pointTotal + " Point");
        }

        else
        {
            System.out.println(pointTotal + " Point");
        }
    }
}
