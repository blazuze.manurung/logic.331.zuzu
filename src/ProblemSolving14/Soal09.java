package ProblemSolving14;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal09 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Menentukan Harga Tiket Parkir");
        System.out.println("1. 8 Jam pertama : 1000/jam");
        System.out.println("2. Lebih dari 8 Jam s.d. 24 Jam : 8000 flat");
        System.out.println("3. Lebih dari 24 Jam : 15000 / 24 Jam");
        System.out.println("Contoh Input Tanggal : 28 January 2020 07:30:34");
        System.out.println("Masukkan Tanggal Masuk : ");
        String masuk = input.nextLine();

        System.out.println("Masukkan Tanggal Keluar : ");
        String keluar = input.nextLine();

        Date masukDate = null;
        Date keluarDate = null;

        DateFormat df = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        try {
            masukDate = df.parse(masuk);
        } catch (ParseException e) {
            System.out.println("Waktu Masuk yang Diinputkan Salah");
        }

        try {
            keluarDate = df.parse(keluar);
        } catch (ParseException e) {
            System.out.println("Waktu Keluar yang Diinputkan Salah");
        }

//         dalam miliseconds
        long bedaWaktu = keluarDate.getTime() - masukDate.getTime();
//        System.out.println(bedaWaktu);

//         milisecond -> second (/1000)
//         second -> minute (/60)
//         minute -> hour (/60)
        long jamBeda = bedaWaktu / (1000 * 60 * 60);
        System.out.println("Lama Anda Parkir : " + jamBeda + " Jam");

        int hargaParkir = 0;
        int berapaHari = 0;

        if(jamBeda <= 8)
        {
            hargaParkir = (int) jamBeda * 1000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        }

        else if (jamBeda > 8 && jamBeda <= 24)
        {
            hargaParkir = 8000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        }
        else
        {
            berapaHari = (int) jamBeda / 24;
            hargaParkir = berapaHari * 15000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        }
    }
}
//        if(masuk.contains("januari"))
//        {
//            masuk.replace("januari", "january");
//        }else if(masuk.contains("februari"))
//        {
//            masuk.replace("februari", "february");
//        }else if(masuk.contains("maret"))
//        {
//            masuk.replace("maret", "march");
//        }else if(masuk.contains("mei"))
//        {
//            masuk.replace("mei", "may");
//        }else if(masuk.contains("juni"))
//        {
//            masuk.replace("juni", "june");
//        }else if(masuk.contains("juli"))
//        {
//            masuk.replace("juli", "july");
//        }else if(masuk.contains("agustus"))
//        {
//            masuk.replace("agustus", "august");
//        }else if(masuk.contains("oktober"))
//        {
//            masuk.replace("oktober", "october");
//        }else if(masuk.contains("desember"))
//        {
//            masuk.replace("desember", "december");
//        }