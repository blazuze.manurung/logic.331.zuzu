//package ProblemSolving14;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Scanner;
//
//public class Soal05 {
//    public static Scanner input = new Scanner(System.in);
//    public static void Resolve()  {
//        System.out.println("Mengubah Data Input menjadi Format 12 Jam atau 24 Jam dan Sebaliknya");
//        System.out.println("Contoh Input Jam : 12:35 AM");
//        System.out.println("Contoh Input Jam : 19:35");
//        System.out.println("Masukkan Data Jam yang ingin Dikonversi : ");
//        String inputJam = input.nextLine();
//
//        Date inputDate = null;
//        String output = null;
//
//        DateFormat dfDuaBelas = new SimpleDateFormat("hh:mm aa");
//        DateFormat dfDuaEmpat = new SimpleDateFormat("HH:mm");
//
//        if (inputJam.length() > 6)
//        {
//            try {
//                inputDate = dfDuaBelas.parse(inputJam);
//            } catch (ParseException e) {
//                throw new RuntimeException(e);
//            }
//            output = dfDuaEmpat.format(inputDate);
//            System.out.println(output);
//        }
//
//        else
//        {
//            try {
//                inputDate = dfDuaEmpat.parse(inputJam);
//            } catch (ParseException e) {
//                throw new RuntimeException(e);
//            }
//            output = dfDuaBelas.format(inputDate);
//            System.out.println(output);
//        }
//    }
//}
