package ProblemSolving14;

import java.util.*;

public class Soal03 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Menghitung Record Penjualan");
        System.out.println("Contoh input : Apel:1, Pisang:3, Jeruk:1");
        System.out.println("Masukkan Data Penjualan Buah");
        String textPenjualan = input.nextLine();

        // Deklarasi
        String[] textPenjualanArray = textPenjualan.split(", ");
        String [] items = new String[2];
        HashMap<String, Integer> mappingData = new HashMap<String, Integer>();

        int helper = 0;

        for (int i = 0; i < textPenjualanArray.length; i++) {
            items = textPenjualanArray[i].split(":");
//           mappingData.put(items[0],Integer.parseInt(items[1]));
            if (mappingData.get(items[0]) == null) {
                mappingData.put(items[0], Integer.parseInt(items[1]));
            } else {
                helper = mappingData.get(items[0]);
                mappingData.put(items[0], Integer.parseInt(items[1]) + helper);
            }
          //  System.out.println(mappingData);
        }

//        Map<String, Integer> map = new HashMap<>(mappingData);
//        TreeMap<String, Integer> sorted = new TreeMap<>(map);
        String[] results = new String[mappingData.size()];

        int index = 0;

        for (String key :  mappingData.keySet())
        {
            results[index] = key +": " + mappingData.get(key);
            index++;
        }

        Arrays.sort(results);

        for (int i = 0; i < mappingData.size(); i++) {
            System.out.println(results[i]);
        }
    }
}
