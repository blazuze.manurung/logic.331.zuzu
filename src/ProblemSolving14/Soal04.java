package ProblemSolving14;

import java.util.HashMap;
import java.util.Scanner;

public class Soal04 {

    public static Scanner input = new Scanner(System.in);

    public static void Resolve() {
        System.out.println("Masukkan Uang Andi : ");
        int uang = input.nextInt();

        System.out.println("Jumlah Barang : ");
        int jumlahBarang = input.nextInt();

        String[] barang = new String[jumlahBarang];
        int [] hargaBarang = new int[jumlahBarang];

        input.nextLine();
        for (int i = 0; i < jumlahBarang; i++) {
            System.out.println("Masukkan Nama Barang " + (i + 1) + " : ");
            barang[i] = input.nextLine();
            System.out.println("Masukkan Harga Barang " + (i + 1) + " : ");
            hargaBarang[i] = input.nextInt();
            input.nextLine();
        }

        for (int i = 0; i < jumlahBarang; i++) {
            if(uang >= hargaBarang[i])
            {
                System.out.print(barang[i] + ", ");
                uang -= hargaBarang[i];
            }
        }
    }
}
