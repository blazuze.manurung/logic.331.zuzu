package ProblemSolving14;

import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {
    public static Scanner input = new Scanner(System.in);

    public static void Resolve()
    {
        System.out.println("Contoh Input Jumlah Uang : 70");
        System.out.println("Masukkan Jumlah Uang : ");
        int uang = input.nextInt();

        input.nextLine();
        System.out.println("Contoh Input Semua Harga Kacamata : 34, 26, 44");
        System.out.println("Masukkan Semua Harga Kacamata : ");
        String textKacamata = input.nextLine();

        System.out.println("Contoh Input Semua Harga Baju : 21, 39, 33");
        System.out.println("Masukkan Semua Harga Baju : ");
        String textBaju = input.nextLine();

        String[] hargaKacamataArray = textKacamata.split(", ");
        int[] intHargaKacamataArray = new int[hargaKacamataArray.length];

        for (int i = 0; i < hargaKacamataArray.length; i++) {
            intHargaKacamataArray[i] = Integer.parseInt(hargaKacamataArray[i]);
        }

        String[] hargaBajuArray = textBaju.split(", ");
        int[] intHargaBajuArray = new int[hargaBajuArray.length];

        for (int i = 0; i < hargaBajuArray.length; i++) {
            intHargaBajuArray[i] = Integer.parseInt(hargaBajuArray[i]);
        }

        int[] hargaSemua = new int[hargaBajuArray.length * hargaKacamataArray.length];

        int helper = 0;
        int helperCariUang = 0;

        for (int i = 0; i < intHargaBajuArray.length; i++) {
            for (int j = 0; j < intHargaKacamataArray.length; j++) {
                hargaSemua[helper] = intHargaKacamataArray[j] + intHargaBajuArray[i];
                helper++;
            }
        }

        Arrays.sort(hargaSemua);

        for (int i = 0; i < hargaSemua.length; i++) {
//            System.out.print(hargaSemua[i] + ", ");

            if(hargaSemua[i] <= uang)
            {
                helperCariUang = hargaSemua[i];
            }
        }

        System.out.println("Gabungan Kacamata dan Baju Termahal yang Bisa Dibeli = " + helperCariUang);
    }
}
