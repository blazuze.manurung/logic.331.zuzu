package ProblemSolving14;

import java.util.Scanner;

public class Soal07 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("- Melambangkan Jalan");
        System.out.println("o Melambangkan Lubang");
        System.out.println("Contoh Input : -----o----o----o- ");
        System.out.println("Masukkan Pola Lintasan : ");
        String dataJalan = input.nextLine();

        System.out.println("w Melambangkan Jalan");
        System.out.println("j Melambangkan Lompat");
        System.out.println("Contoh Input : wwwwwjwwwjwwwj ");
        System.out.println("Masukkan Pilihan Cara Jalan : ");
        String dataCaraJalan = input.nextLine().toLowerCase();

        int energi = 0;
        boolean jatuh = false;
        int helper = 0;

        for (int i = 0; i < dataCaraJalan.length(); i++) {
            if (dataCaraJalan.charAt(i) == 'w' && dataJalan.charAt(helper) == '-')
            {
                energi++;
                helper++;
            }
            else if (dataCaraJalan.charAt(i) == 'w' && dataJalan.charAt(helper) == 'o')
            {
                jatuh = true;
                break;
            }
            else if (dataCaraJalan.charAt(i) == 'j' && energi >= 2 )
            {
                helper += 2;
                if(dataJalan.charAt(helper - 1) == 'o')
                {
                    jatuh = true;
                    break;
                }
                else
                {
                    energi -= 2;
                }
            }
            else if (dataCaraJalan.charAt(i) == 'j' && dataJalan.charAt(helper) == 'o' && energi < 2)
            {
                jatuh = true;
                break;
            }

//            if (dataJalan.charAt(i) == 'o' && dataCaraJalan.charAt(i) == 'j' && energi < 0)
//            {
//                jatuh = true;
//                break;
//            }
//            else if (dataJalan.charAt(i) == '-' && dataCaraJalan.charAt(i) == 'j' && dataJalan.charAt(i+1) == 'o' )
//            {
//                jatuh = true;
//                break;
//            }
        }

        if(jatuh)
        {
            System.out.println("Jim Died");
        }

        if (!jatuh)
        {
            System.out.println(energi);
        }
    }
}
