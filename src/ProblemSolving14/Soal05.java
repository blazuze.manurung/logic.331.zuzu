package ProblemSolving14;

import java.util.Scanner;

public class Soal05 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Mengubah Data Input menjadi Format 12 Jam atau 24 Jam dan Sebaliknya");
        System.out.println("Contoh Input Jam : 12:35 AM");
        System.out.println("Contoh Input Jam : 19:35");
        System.out.println("Masukkan Data Jam yang ingin Dikonversi : ");
        String inputJam = input.nextLine().toUpperCase();

        String jamString = inputJam.substring(0,2);
        int jam = Integer.parseInt(jamString);

        if (inputJam.contains("PM"))
        {
            jam += 12;
            String balikJam = Integer.toString(jam);
            if (jam > 24)
            {
                System.out.println("Input Jam Salah");
            }
            else if (jam == 24)
            {
                inputJam = inputJam.replace(jamString, "00");
                inputJam = inputJam.replace("PM", "");
                System.out.println(inputJam);
            }
            else
            {
                inputJam = inputJam.replace(jamString, balikJam);
                inputJam = inputJam.replace("PM", "");
                System.out.println(inputJam);
            }
        }
        else if (inputJam.contains("AM"))
        {
            if (jam < 12)
            {
                inputJam = inputJam.replace("AM", "");
                System.out.println(inputJam);
            }
            else if (jam == 12)
            {
                jam -= 12;
                String balikJam = Integer.toString(jam);
                inputJam = inputJam.replace(jamString, balikJam);
                inputJam = inputJam.replace("AM", "");
                System.out.println("0" + inputJam);
            }
            else
            {
                System.out.println("Input Jam Salah");
            }
        }

        else
        {
           if (jam >= 12 && jam < 24)
           {
               jam -= 12;
               String balikJam = Integer.toString(jam);
               inputJam = inputJam.replace(jamString, balikJam);
               System.out.println("0" + inputJam + " PM");
           }
           else if (jam >= 24)
           {
               System.out.println("Input Jam Salah");
           }
           else
           {
               System.out.println(inputJam + " AM");
           }
        }
    }
}
