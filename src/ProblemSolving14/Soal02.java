package ProblemSolving14;

import java.util.Scanner;

public class Soal02 {
    public static Scanner input = new Scanner(System.in);

    public static void Resolve()
    {
        System.out.println("Daftar Toko dan Jarak");
        System.out.println("Toko 1 : 0.5 Km");
        System.out.println("Toko 2 : 2 Km");
        System.out.println("Toko 3 : 3.5 Km");
        System.out.println("Toko 4 : 5 Km");
        System.out.println("Contoh input : 1-2-3-4");
        System.out.println("Masukkan Urutan Toko yang Dikunjungi : ");
        String text = input.nextLine();
        String jarakAntarToko = "0 0.5 2 3.5 5";

        int[] intKunjungi = Utility.ConvertStringToArrayInt(text);
        double[] jarakToko = Utility.ConvertStringToArrayDouble(jarakAntarToko);

        double jarakTempuh = 0;
        double perbandinganJarak = 0;
        int menit = 0;

        for (int i = 0; i < intKunjungi.length ; i++) {
            if(i == 0)
            {
                jarakTempuh = jarakToko[intKunjungi[i]];
            }

            else if (i == intKunjungi.length - 1)
            {
                perbandinganJarak = jarakToko[intKunjungi[i]] - jarakToko[intKunjungi[i-1]];
                if(perbandinganJarak < 0)
                {
                    perbandinganJarak *= -1;
                }
                jarakTempuh = jarakTempuh + perbandinganJarak + jarakToko[intKunjungi[i]];
            }

            else
            {
                perbandinganJarak = jarakToko[intKunjungi[i]] - jarakToko[intKunjungi[i-1]];
                if(perbandinganJarak < 0)
                {
                    perbandinganJarak *= -1;
                }
                jarakTempuh = jarakTempuh + perbandinganJarak;
            }
        }

        menit = (int) (jarakTempuh * 2) + (intKunjungi.length * 10);
        System.out.println(menit + " menit");

        System.out.println("Total Jarak Tempuh : " + jarakTempuh + " Km");
    }
}
