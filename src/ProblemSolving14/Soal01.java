package ProblemSolving14;

import java.util.Scanner;

public class Soal01 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve(){

        System.out.println("Input Panjang Deret : ");
        int panjangDeret = input.nextInt();

        int[] deretPertama = new int[panjangDeret];
        int[] deretKedua = new int[panjangDeret];
        int[] deretJumlah = new int[panjangDeret];


        System.out.print("Deret Pertama : ");
        for (int i = 0; i < panjangDeret; i++) {
            deretPertama[i] = (i * 3) - 1;

            System.out.print(deretPertama[i] + " ");
        }

        System.out.println();
        System.out.print("Deret Kedua : ");
        for (int i = 0; i < panjangDeret; i++) {
            deretKedua[i] = (i * (-2));
            System.out.print(deretKedua[i] + " ");
        }

        System.out.println();
        System.out.print("Output : ");
        for (int i = 0; i < panjangDeret; i++) {
            deretJumlah[i] = deretPertama[i] + deretKedua[i];
            System.out.print(deretJumlah[i] + " ");
        }
    }
}
