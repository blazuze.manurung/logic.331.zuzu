package ProblemSolving14;

import java.util.Scanner;

public class Soal10 {

    public static Scanner input = new Scanner(System.in);
    private static String wadah = "botol, gelas, teko, cangkir";

    public static void Resolve() {
        String[] wadahArrray = wadah.split(", ");

        System.out.println("Konversi Wadah Sesuai Takaran");
        System.out.println("1 botol = 2 gelas");
        System.out.println("1 teko = 25 cangkir");
        System.out.println("1 gelas = 2.5 cangkir");

        System.out.println("Pilihan Wadah : botol, gelas, teko, cangkir");
        System.out.println("Contoh Input : botol");
        System.out.println("Inputkan Wadah yang Ingin Dikonversi : ");
        String inputWadah = input.nextLine();

        while (!inputWadah.equals(wadahArrray[0]) && !inputWadah.equals(wadahArrray[1])
                && !inputWadah.equals(wadahArrray[2]) && !inputWadah.equals(wadahArrray[3]))
        {
            System.out.println("Pilihan Wadah Tidak Tersedia");
            inputWadah = input.nextLine();
        }

        System.out.println("Pilihan Wadah : botol, gelas, teko, cangkir");
        System.out.println("Contoh Input : 2");
        System.out.println("Inputkan Jumlah Wadah :");
        int jumlahWadah = input.nextInt();

        input.nextLine();
        System.out.println("Contoh Input : gelas");
        System.out.println("Inputkan Wadah Setelah Dikonversi : ");
        String inputWadahKonversi = input.nextLine();

        while (inputWadahKonversi.equals(wadahArrray[0]) && inputWadahKonversi.equals(wadahArrray[1])
                && inputWadahKonversi.equals(wadahArrray[2]) && inputWadahKonversi.equals(wadahArrray[3]))
        {
            System.out.println("Pilihan Wadah Tidak Tersedia");
            inputWadahKonversi = input.nextLine();
        }

        double jumlahWadahKonversi = 0;

        for (int i = 0; i < wadahArrray.length; i++) {
            if (inputWadah.equals(wadahArrray[i])) {
                if (i == 0 && inputWadahKonversi.equals(wadahArrray[0])) {
                    jumlahWadahKonversi = jumlahWadah;
                } else if (i == 0 && inputWadahKonversi.equals(wadahArrray[1])) {
                    jumlahWadahKonversi = jumlahWadah * 2;
                } else if (i == 0 && inputWadahKonversi.equals(wadahArrray[2])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 5;
                } else if (i == 0 && inputWadahKonversi.equals(wadahArrray[3])) {
                    jumlahWadahKonversi = jumlahWadah * 5;
                }
                if (i == 1 && inputWadahKonversi.equals(wadahArrray[1])) {
                    jumlahWadahKonversi = jumlahWadah;
                } else if (i == 1 && inputWadahKonversi.equals(wadahArrray[0])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 2;
                } else if (i == 1 && inputWadahKonversi.equals(wadahArrray[2])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 10;
                } else if (i == 1 && inputWadahKonversi.equals(wadahArrray[3])) {
                    jumlahWadahKonversi = (double) jumlahWadah * 2.5;
                }
                if (i == 2 && inputWadahKonversi.equals(wadahArrray[2])) {
                    jumlahWadahKonversi = jumlahWadah;
                } else if (i == 2 && inputWadahKonversi.equals(wadahArrray[0])) {
                    jumlahWadahKonversi = jumlahWadah * 5;
                } else if (i == 2 && inputWadahKonversi.equals(wadahArrray[1])) {
                    jumlahWadahKonversi = jumlahWadah * 10;
                } else if (i == 2 && inputWadahKonversi.equals(wadahArrray[3])) {
                    jumlahWadahKonversi = jumlahWadah * 25;
                }
                if (i == 3 && inputWadahKonversi.equals(wadahArrray[3])) {
                    jumlahWadahKonversi = jumlahWadah;
                } else if (i == 3 && inputWadahKonversi.equals(wadahArrray[0])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 5;
                } else if (i == 3 && inputWadahKonversi.equals(wadahArrray[1])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 2.5;
                } else if (i == 3 && inputWadahKonversi.equals(wadahArrray[2])) {
                    jumlahWadahKonversi = (double) jumlahWadah / 25;
                }
            }
        }
        System.out.println(jumlahWadah + " " + inputWadah + " = " + jumlahWadahKonversi + " " + inputWadahKonversi);
    }
}
