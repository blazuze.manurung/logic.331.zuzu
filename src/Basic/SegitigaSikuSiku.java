package Basic;

import java.util.Scanner;

public class SegitigaSikuSiku {
    private static Scanner input = new Scanner(System.in);
    private static double alas;
    private static double tinggi;
    private static double miring;
    private static double luasSegitiga;
    private static double kelilingSegitiga;
    public static void luas()
    {
        System.out.print("Input Alas Segitiga Siku Siku(cm) = ");
        alas = input.nextDouble();

        System.out.print("Input Tinggi Segitiga Siku Siku(cm) = ");
        tinggi = input.nextDouble();

        luasSegitiga = 0.5 * alas * tinggi;
        System.out.println("Luas Segitiga = " + luasSegitiga + "cm²");
    }

    public static void keliling()
    {
        System.out.print("Input Alas Segitiga Siku Siku (cm) = ");
        alas = input.nextDouble();

        System.out.print("Input Tinggi Segitiga Siku Siku (cm) = ");
        tinggi = input.nextDouble();

        miring = Math.sqrt(Math.pow(alas,2)+Math.pow(tinggi,2));

        kelilingSegitiga = alas + tinggi + miring;
        System.out.println("Keliling Segitiga = " + kelilingSegitiga + "cm");
    }
}
