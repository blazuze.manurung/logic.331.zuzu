package Basic;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);
    private static double sisiAtas;
    private static double sisiBawah;
    private static double sisiMiring;
    private static double tinggi;
    private static double luasTrapesium;
    private static double kelilingTrapesium;

    public static void luas()
    {
        System.out.print("Input Sisi Atas Trapesium (cm) = ");
        sisiAtas = input.nextDouble();

        System.out.print("Input Sisi Bawah Trapesium (cm) = ");
        sisiBawah = input.nextDouble();

        System.out.print("Input Tinggi Trapesium (cm) = ");
        tinggi = input.nextDouble();

        luasTrapesium = 0.5 * (sisiAtas + sisiBawah) * tinggi;

        System.out.println("Luas Trapesium = " + luasTrapesium + " cm²");
    }

    public static void keliling()
    {
        System.out.print("Input Sisi Atas Trapesium (cm) = ");
        sisiAtas = input.nextDouble();

        System.out.print("Input Sisi Bawah Trapesium (cm) = ");
        sisiBawah = input.nextDouble();

        System.out.print("Input Tinggi Trapesium (cm) = ");
        tinggi = input.nextDouble();

        System.out.print("Input Sisi Miring Trapesium (cm) = ");
        sisiMiring = input.nextDouble();

        kelilingTrapesium = sisiAtas+ sisiBawah + sisiMiring + sisiMiring;
        System.out.println("Keliling Trapesium = " + kelilingTrapesium + " cm");
    }
}
