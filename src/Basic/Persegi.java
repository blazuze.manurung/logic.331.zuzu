package Basic;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);
    private static double panjangSisi;
    private static double luasPersegi;
    private static double kelilingPersegi;
    public static void luas()
    {
        System.out.println("Input Panjang Sisi Persegi (cm) = ");

        panjangSisi = input.nextDouble();
        luasPersegi = Math.pow(panjangSisi, 2);

        System.out.println("Luas Persegi = " + luasPersegi + " cm²");
    }

    public static void keliling()
    {
        System.out.println("Input Panjang Sisi Persegi (cm) = ");
        panjangSisi = input.nextDouble();
        kelilingPersegi = 4 * panjangSisi;

        System.out.println("Keliling Persegi = " + kelilingPersegi + " cm");
    }
}
