package Basic;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);
    private static double radius;
    private static double luasLingkaran;
    private static double kelilingLingkaran;

    public static void luas()
    {
        System.out.print("Input Radius Lingkaran (cm) = ");
        radius = input.nextDouble();

        double luasLingkaran = Math.PI * radius * radius;


        System.out.println("Luas Lingkaran = " + luasLingkaran + " cm²");
    }
    public static void keliling()
    {
        System.out.print("Input Radius Lingkaran (cm) = ");
        radius = input.nextDouble();

        double kelilingLingkaran = 2 * Math.PI * radius;
        System.out.println("Keliling Lingkaran = " + kelilingLingkaran + " cm");
    }
}
