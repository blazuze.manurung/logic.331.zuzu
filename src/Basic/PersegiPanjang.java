package Basic;

import java.util.Scanner;

public class PersegiPanjang {
    private static Scanner input = new Scanner(System.in);
    private static double panjang;
    private static double lebar;
    private static double luasPersegiPanjang;
    private static double kelilingPersegiPanjang;
    public static void luas()
    {
        System.out.print("Input Panjang Persegi Panjang (cm) = ");
        panjang = input.nextDouble();

        System.out.print("Input Lebar Persegi Panjang (cm) = ");
        lebar = input.nextDouble();

        luasPersegiPanjang = panjang * lebar;

        System.out.println("Luas Persegi Panjang adalah = " + luasPersegiPanjang + " cm²");

    }
    public static void keliling()
    {
        System.out.print("Input Panjang Persegi Panjang (cm) = ");
        panjang = input.nextDouble();

        System.out.print("Input Lebar Persegi Panjang (cm) = ");
        lebar = input.nextDouble();

        kelilingPersegiPanjang = (2 * panjang) + (2 * lebar);
        System.out.println("Keliling Persegi Panjang adalah = " + kelilingPersegiPanjang + " cm");
    }
}
