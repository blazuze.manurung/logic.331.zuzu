package WarmUp;

import java.util.Scanner;

public class CompareTheTriplets {
    private static Scanner input = new Scanner(System.in);
    public static void  Resolve()
    {
        System.out.println("===Compare the Triplets===");
        System.out.println("Masukkan data yang ingin di bandingkan (banyak kedua data harus sama)");
        System.out.println("Data 1 : ");
        String textPertama = input.nextLine();
        System.out.println("Data 2 : ");
        String textKedua = input.nextLine();
        int hitungDataPertama = 0;
        int hitungDataKedua = 0;
        int hitungSama = 0;

        int[] dataPertama = Utility.ConvertStringToArrayInt(textPertama);
        int[] dataKedua = Utility.ConvertStringToArrayInt(textKedua);

        for (int i = 0; i < dataPertama.length; i++) {
            if (dataPertama[i] > dataKedua[i])
            {
                hitungDataPertama++;
            }
            else if(dataPertama[i] < dataKedua[i])
            {
                hitungDataKedua++;
            }
            else if (dataPertama[i] == dataKedua[i])
            {
                hitungSama++;
            }
        }
        System.out.println("Banyaknya data yang lebih tinggi dari data lainnya");
        System.out.println("Data 1 : " + hitungDataPertama);
        System.out.println("Data 2 : " + hitungDataKedua);
        System.out.println("Data yang Sama : " + hitungSama);
    }
}
