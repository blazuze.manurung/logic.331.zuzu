package WarmUp;

public class Utility {
    public static int[] ConvertStringToArrayInt (String text){

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++)
        {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        return intArray;
    }
    public static void PrintArray2DBintang(int[][] hasil)
    {
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil.length; j++) {
                if (hasil [i][j] == 0)
                {
                    System.out.print("  ");
                }
                else
                {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    }
    public static long[] ConvertStringToArrayLong(String text){

        String[] textArray = text.split(" ");
        long[] longArray = new long[textArray.length];

        for (int i = 0; i < textArray.length; i++)
        {
            longArray[i] = Long.parseLong(textArray[i]);
        }
        return longArray;
    }
}
