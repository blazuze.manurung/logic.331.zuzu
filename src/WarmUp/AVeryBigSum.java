package WarmUp;

import java.util.Scanner;

public class AVeryBigSum {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("===A Very Big Sum===");
        System.out.println("Tolong Masukkan Deret Angka dengan banyak data lebih dari 32bit");
        String text = input.nextLine();

        long[] longArray = Utility.ConvertStringToArrayLong(text);
        int length = longArray.length;

        long jumlah = 0;

        for (int i = 0; i < length; i++) {
            jumlah += longArray[i];
        }
        System.out.println(jumlah);
    }
}
