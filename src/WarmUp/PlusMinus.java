package WarmUp;

import java.text.DecimalFormat;
import java.util.Scanner;

public class PlusMinus {
    public static void Resolve()
    {

        Scanner input = new Scanner(System.in);

        System.out.println("===Plus Minus===");
        System.out.println("Menghitung Persentase Banyaknya Data Positive, Negative, dan Zero");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitungPositive = 0;
        int hitungNol = 0;
        int hitungNegative = 0;
        double persenPositive = 0;
        double persenNegative = 0;
        double persenNol = 0;

        for (int i = 0; i < length; i++) {
            if (intArray[i] == 0)
            {
                hitungNol++;
            }
            else if (intArray[i] > 0)
            {
                hitungPositive++;
            }
            else
            {
                hitungNegative++;
            }

        }
        persenPositive = (double) hitungPositive / length;
        persenNegative = (double) hitungNegative / length;
        persenNol = (double) hitungNol / length;

        System.out.println("Positive = " + persenPositive);
        System.out.println("Negative = " + persenNegative);
        System.out.println("Zero= " + persenNol);
    }
}
