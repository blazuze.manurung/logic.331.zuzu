package WarmUp;

import javax.script.ScriptContext;
import java.sql.SQLOutput;
import java.util.Scanner;

public class DiagonalDifference {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("===Diagonal Difference===");
        System.out.println("Membandingkan 9 data miring kiri-kanan dengan miring kanan-kiri");
        System.out.println("Contoh input data : ");
        System.out.println("1 5 8");
        System.out.println("9 3 2");
        System.out.println("4 5 7");

        System.out.println("Masukkan data : ");
        System.out.println("Data Baris 1 :  ");
        String textSatu = input.nextLine();
        System.out.println("Data Baris 2 :  ");
        String textDua = input.nextLine();
        System.out.println("Data Baris 3 :  ");
        String textTiga = input.nextLine();

        int[] dataSatu = Utility.ConvertStringToArrayInt(textSatu);
        int[] dataDua = Utility.ConvertStringToArrayInt(textDua);
        int[] dataTiga = Utility.ConvertStringToArrayInt(textTiga);

        int hasil = 0;

        int nilaiA = dataSatu[0] + dataDua[1] + dataTiga[2];
        int nilaiB = dataSatu[2] + dataDua[1] + dataTiga[0];
        if (nilaiA > nilaiB){
            hasil = nilaiA - nilaiB;
        } else {
            hasil = nilaiB - nilaiA;
        }
        System.out.println("Selisih = " + hasil);
    }
}
