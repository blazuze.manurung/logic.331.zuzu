package WarmUp;


import InputPlural.Soal01;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        System.out.println("Pilih Nomor Soal (1-10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10)
        {
            System.out.println("Nomor Soal Tidak Tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1: SolveMeFirst.Resolve();
                break;
//            case 2: TimeConversion.Resolve();
//                break;
            case 3: SimpleArraySum.Resolve();
                break;
            case 4: DiagonalDifference.Resolve();
                break;
            case 5: PlusMinus.Resolve();
                break;
            case 6: Staircase.Resolve();
                break;
            case 7: MiniMaxSum.Resolve();
                break;
            case 8: BirthdayCakeCandles.Resolve();
                break;
            case 9: AVeryBigSum.Resolve();
                break;
            case 10: CompareTheTriplets.Resolve();
                break;
        }
    }
}