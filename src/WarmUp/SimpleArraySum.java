package WarmUp;

import java.util.Scanner;

public class SimpleArraySum {

    private static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {

        System.out.println("===Simple Array Sum===");
        System.out.println("Menjumlahkan Semua Data yang Diinputkan");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();
        int jumlah = 0;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
            jumlah += intArray[i];
        }
        System.out.println(jumlah);
    }
}
