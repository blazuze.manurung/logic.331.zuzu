package FinalPractice;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Cupcake10 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        double terigu = (double) 125 / 15;
        double gula = (double) 100 / 15;
        double susu = (double) 100 /15;

        System.out.println("Berapa Cupcake yang Ingin Dibuat : ");
        int jumlah = input.nextInt();

        double jumlahTerigu = terigu * jumlah;
        double jumlahGula = gula * jumlah;
        double jumlahSusu = susu * jumlah;

        DecimalFormat df = new DecimalFormat("#####");

        System.out.println("Terigu : " + df.format(jumlahTerigu) + " gr");
        System.out.println("Gula Pasir : " + df.format(jumlahGula) + " gr");
        System.out.println("Susu : " + df.format(jumlahSusu) + " mL");
    }
}
