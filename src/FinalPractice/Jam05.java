package FinalPractice;

import java.util.Scanner;

public class Jam05 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Contoh input : 03:40:44 PM");
        System.out.println("Masukkan Data Jam : ");
        String data = input.nextLine().toUpperCase();

        String jam = data.substring(0,2);
        int jamInt = Integer.parseInt(jam);

        String menit = data.substring(3,5);
        int menitInt = Integer.parseInt(menit);

        String detik = data.substring(6,8);
        int detikInt = Integer.parseInt(detik);

        while (jamInt == 0 || jamInt >= 12 || menitInt > 60 || detikInt > 60 )
        {
            System.out.println("Input detik / menit salah");
            System.out.println("Masukkan Data Jam : ");
            data = input.nextLine();

            jam = data.substring(0,2);
            jamInt = Integer.parseInt(jam);

            menit = data.substring(3,5);
            menitInt = Integer.parseInt(menit);

            detik = data.substring(6,8);
            detikInt = Integer.parseInt(detik);

        }

        if (data.contains("PM"))
        {
            jamInt += 12;
            String balikJam = String.valueOf(jamInt);
            data = data.replace(jam, balikJam);
            data = data.replace("PM", "");
            System.out.println(data);
        }

        else
        {
            data = data.replace("AM", "");
            System.out.println(data);
        }

    }
}
