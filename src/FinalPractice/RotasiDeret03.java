package FinalPractice;


import java.util.Scanner;

public class RotasiDeret03 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Contoh : 7 3 9 9 2");
        System.out.println("Masukkan Deret Angka : ");
        String data = input.nextLine();
        int temp = 0;
        String[] dataArray = data.split(" ");
        System.out.println("Masukkan Berapa Kali Rotasi : ");
        int rotasi = input.nextInt();

        for (int i = 0; i < rotasi; i++) {
            System.out.print("Rotasi " + (i + 1) + " : ");
            temp = Integer.parseInt(dataArray[0]);
            for (int j = 0; j < dataArray.length; j++) {
                if (j == dataArray.length - 1)
                {
                    dataArray[j] = String.valueOf(temp);
                    System.out.print(dataArray[j] + " ");
                }
                else
                {
                    dataArray[j] = dataArray[j+1];
                    System.out.print(dataArray[j] + " ");
                }
            }
            System.out.println();
        }
    }
}
