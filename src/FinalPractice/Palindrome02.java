package FinalPractice;

import java.util.Scanner;

public class Palindrome02 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("=== Palindrome ===");
        System.out.println("Masukkan data untuk mengecek Palindrome atau tidak : ");
        String data = input.nextLine().toLowerCase();

        StringBuilder dataReverse = new StringBuilder();
        dataReverse.append(data);
        dataReverse.reverse();
        String dataReverseString = dataReverse.toString();

//        System.out.println(dataReverse);

        if (!dataReverseString.equals(data)) {
            System.out.println("BUKAN Palindrome");
        } else {
            System.out.println("Palindrome");
        }
    }
}
