package FinalPractice;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SudutJam09 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("=====> Menghitung Derajat Jarum Jam <=====");
        System.out.println("Format input : 03:00 tidak lebih dari jam 12.00");
        System.out.println("Masukkan jam : ");
        String inputJam = input.nextLine();
        String[] splitInput = inputJam.split(":");
        double jam = Double.parseDouble(splitInput[0]);
        double menit = Double.parseDouble(splitInput[1]);
        double arahMenit = (menit/60) * 12;
        double nilaiSudutPerJam = 30;

        double output = 0;
        if (menit > 0){
            jam += (menit / 60);
        }
        if (jam >= 12){
            jam -= 12;
        }

        if (jam > arahMenit){
            output = (jam - arahMenit) * nilaiSudutPerJam;
        } else if (jam < arahMenit) {
            output = (arahMenit - jam) * nilaiSudutPerJam;
        }

        if (output > 180){
            output = 360 - output;
        }

        DecimalFormat df = new DecimalFormat("0.00");

        System.out.println("Output = " + df.format(output) + " Derajat");
    }
}
