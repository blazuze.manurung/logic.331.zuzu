package FinalPractice;

import java.util.Scanner;

public class DeretAngka07 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Contoh Input : 8 7 0 2 7 1 7 6 3 0 7 1 3 4 6");
        System.out.println("Masukkan Deret Angka : ");
        String data = input.nextLine();
        String[] dataArray = data.split(" ");
        int[] dataIntArray = new int[dataArray.length];

        for (int i = 0; i < dataArray.length; i++) {
            dataIntArray[i] = Integer.parseInt(dataArray[i]);
        }

        int jumlah = 0;
        for (int i = 0; i < dataIntArray.length; i++) {
            jumlah += dataIntArray[i];
        }

        double mean = (double) jumlah / dataIntArray.length;
        System.out.println("Mean : " + mean);

        boolean genap = true;
        if (dataIntArray.length % 2 == 0)
        {
            genap = true;
        }
        else
        {
            genap = false;
        }
        double median = 0;
        if(genap)
        {
            median = (double) (dataIntArray[dataIntArray.length/2] + dataIntArray[(dataIntArray.length/2) - 1] / 2);
        }
        else
        {
            median = dataIntArray[dataIntArray.length/2];
        }
        System.out.println("Median : " + median);


    }

}
