package BankBBF;

import java.util.Scanner;

public class BuatPin {
    private static String pinBaru;
    private static Scanner input = new Scanner(System.in);

    public static void buatPIN()
    {
        boolean flag = true;

        while (flag)
        {
            System.out.println("Pembuatan PIN Baru");
            System.out.println("1. PIN harus terdiri dari 6 Angka");
            System.out.println("2. PIN tidak boleh character selain angka");
            System.out.print("Silahkan masukkan PIN Baru : ");
            pinBaru = input.next();
            Validasi.validasiAngka(pinBaru);
            if (pinBaru.length() != 6)
            {
                System.out.println("Jumlah angka tidak sesuai!");
                System.out.print("Tolong Ulangi ");
            }
            else
            {
                System.out.println("Pembuatan PIN Baru Telah Berhasil!");
                flag = false;
            }
        }
    }
    public static String getBuatPin(){
        return pinBaru;
    }
}

