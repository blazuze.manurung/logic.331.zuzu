package BankBBF;

import java.util.Scanner;

public class Menu
{
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    public static void menuTransaksi()
    {
        while(flag)
        {
            System.out.println("===Menu Transaksi===");
            System.out.println("1. Setoran Tunai");
            System.out.println("2. Transfer");
            System.out.println("3. Exit");
            System.out.println("====================");
            int menuPilihan = input.nextInt();

            if (menuPilihan == 1)
            {
                SetoranTunai.setorUang();
                flag = false;
            }
            else if (menuPilihan == 2)
            {
                Transfer.trasferUang();
                flag = false;
            }
            else
            {
                flag = false;
                System.out.println("Terima Kasih Sudah Menggunakan Bank Kami!");
                System.out.println("Salam Hangat dari Bapak Faiq");
                break;
            }
        }
    }
}
