package BankBBF;

public class Validasi {
    public static boolean validasiAngka(String input) {
        if (input == null)
        {
            return false;
        }

        try
        {
            // Validation dibawah digunakan untu mengecek bilangan bulat
            int validation = Integer.parseInt(input);
            return true; //kalo validationnya tidak error, gak perlu catch
        }

        catch (NumberFormatException exception)
        {
            System.out.println("Mohon maaf, hanya inputkan angka saja!");
            System.out.print("Tolong Ulangi ");
            return false;
        }
    }
}
