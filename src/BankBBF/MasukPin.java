package BankBBF;

import java.util.Scanner;

public class MasukPin {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    public static void inputPin()
    {
        String pinBaru = BuatPin.getBuatPin();

        int hitungSalah = 0;

        flag = true;

        while(flag)
        {
            if (hitungSalah == 3)
            {
                System.out.println("Mohon Maaf Akun Anda Terblokir");
                flag = false;
                break;
            }

            System.out.print("Silahkan Masukkan PIN : ");
            String pinLama = input.next();
            Validasi.validasiAngka(pinLama);

            if (pinLama.equals(pinBaru))
            {
                flag = false;
                Menu.menuTransaksi();
            }

            if (!pinLama.equals(pinBaru))
            {
                System.out.println("Mohon Maaf PIN yang Anda Masukkan Salah");
                System.out.println("Silahkan Masukkan PIN Kembali");
                hitungSalah++;
                System.out.println("Kesempatan Tersisa " + (3 - hitungSalah) +
                        ", Jika Kesempatan Habis Akun Akan Terblokir");
            }
        }
    }
}
