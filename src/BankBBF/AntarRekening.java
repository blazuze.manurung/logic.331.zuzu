package BankBBF;

import java.util.Scanner;
import java.util.Set;

public class AntarRekening {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag;
    private static boolean flag2;
    public static void trasnferRekening ()
    {
        int saldo = SetoranTunai.getSaldo();
        flag = true;
        flag2 = true;

        while (flag)
        {
            System.out.println("Transfer Antar Rekening");
            System.out.println("Ketentuan Nomor Rekening Adalah 7 Angka");
            System.out.println("Masukkan Nomor Rekening Tujuan : ");
            String nomorRekening = input.next();
            Validasi.validasiAngka(nomorRekening);
            if (nomorRekening.length() != 7)
            {
                System.out.println("Jumlah Angka Tidak Sesuai!");
                System.out.print("Ulangi ");
            }
            else
            {
                while (flag2)
                {
                    System.out.println("Masukkan Jumlah Uang yang Ingin Ditransfer : ");
                    String cekUangTrasfer = input.next();
                   if (Validasi.validasiAngka(cekUangTrasfer)){
                       int uangTrasfer = Integer.parseInt(cekUangTrasfer);
                       if (uangTrasfer > saldo)
                       {
                           System.out.println("Mohon Maaf Saldo Anda Tidak Cukup Alias Anda Miskin!");
                           System.out.print("Tolong Ulangi ");
                       }
                       else {
                           System.out.println("Transaksi Anda Berhasil!");
                           saldo -= uangTrasfer;
                           SetoranTunai.setSaldo(saldo);
                           System.out.println("Saldo anda tersisa Rp " + saldo);
                           System.out.println("Apakah Anda Ingin Melanjutkan Transaksi? (y/n)");
                           input.nextLine();
                           String jawabanLanjut = input.nextLine();
                           if (!jawabanLanjut.toLowerCase().equals("y")) {
                               flag2 = false;
                               flag = false;
                               break;
                           } else {
                               flag2 = false;
                               flag = false;
                               MasukPin.inputPin();
                           }
                       }
                   }
                }
            }
        }
    }
}
