package BankBBF;

import java.util.Scanner;

public class SetoranTunai {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    private static int saldo;

    public static void setorUang() {
        System.out.println("Silahkan Ketik Nominal Uang (tanpa titik)");
        System.out.println("Batas Setor Uang Hari Ini Rp 25.000.000");
        flag = true;

        while (flag) {
            System.out.print("Jumlah Setor Uang : Rp ");
            String cekUangSetor = input.next();
            if (Validasi.validasiAngka(cekUangSetor))
            {
                int uangSetor = Integer.parseInt(cekUangSetor);
                if (uangSetor >= 25000000) {
                    System.out.println("Mohon Maaf Batas Setor Uang Harian Rp 25.000.000");
                    System.out.println("Silahkan Masukkan Jumlah Setor Uang Kembali");
                } else {
                    System.out.println("Are you sure? (y/n)");
                    input.nextLine(); //skip bugged
                    String jawaban = input.nextLine();
                    if (!jawaban.toLowerCase().equals("y")) {
                        System.out.println("Silahkan Masukkan Jumlah Setor Uang Kembali");

                    } else {
                        System.out.println("Anda Telah Berhasil Setor Uang!");
                        saldo = uangSetor;
                        System.out.println("Jumlah Uang Saat Ini = Rp " + uangSetor);
                        System.out.println("Apakah Anda Ingin Melanjutkan Transaksi? (y/n)");
                        String jawabanLanjut = input.nextLine();
                        if (!jawabanLanjut.toLowerCase().equals("y")) {
                            flag = false;
                            break;
                        } else {
                            flag = false;
                            MasukPin.inputPin();
                        }
                    }
                }
            }
        }
    }
    public static int getSaldo()
    {
        return saldo;
    }
    public static void setSaldo(int saldo)
    {
        SetoranTunai.saldo = saldo;
    }
}

