package InputPlural;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Menghitung Penjumlahan Terbesar dan Terkecil dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;
        int hitungTerbesar = 0;
        int hitungTerkecil = 0;
        int palingBesar = 0;
        int[] hitungUrutan = new int[length];

        // perulangan di bawah di gunakan untuk mencari nilai urutan
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (intArray[i] > intArray[j]) {
                    hitung++;
                }
            }
            hitungUrutan[i] = hitung;
//            System.out.println(hitungUrutan[i]);
            hitung = 0;
        }

        for (int i = 0; i < length; i++) {
            if (palingBesar < hitungUrutan[i])
            {
                palingBesar = hitungUrutan[i];
            }
        }
        for (int i = 0; i < length; i++) {
            if (hitungUrutan[i] < palingBesar)
            {
                hitungTerkecil += intArray[i];
            }
        }

        for (int i = 0; i < length; i++) {
            if (hitungUrutan[i] >= 1)
            {
                hitungTerbesar += intArray[i];
            }
        }
        System.out.println("Terkecil : " + hitungTerkecil);
        System.out.println("Terbesar : " + hitungTerbesar);
        }
    }

