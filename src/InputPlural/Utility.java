package InputPlural;

public class Utility {
    public static int[] ConvertStringToArrayInt (String text){

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++)
        {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        return intArray;
    }
    public static char[] ConvertStringToArraychar (String text){

        String[] textArray = text.split(" ");
        char[] charArray = new char[textArray.length];

        for (int i = 0; i < textArray.length; i++)
        {
            charArray[i] = textArray[i].charAt(0);
        }

        return charArray;
    }
}
