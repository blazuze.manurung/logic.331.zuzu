package InputPlural;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Mencari Median dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        boolean median = true;

        if (length % 2 == 0)
        {
            median = true;
        }
        else
        {
            median = false;
        }

        double nilaiMedian = 0;

        for (int i = 0; i < length; i++) {
            if (median)
            {
                nilaiMedian = (double) (intArray[length / 2] +  intArray[(length / 2) - 1]) / 2;
            }
            else
            {
                nilaiMedian = intArray[length / 2];
            }
        }
        System.out.println(nilaiMedian);
    }
}
