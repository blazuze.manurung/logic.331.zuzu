package InputPlural;

import java.util.Scanner;

public class Soal04 {
   public static void Resolve (){

        Scanner input = new Scanner(System.in);

        System.out.println("Mencari Modus dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitungSama = 0;
        boolean samaAtauTidak = false;
        int[] arrayHitungSama = new int[length];
        int hitungPalingBesar = 0;

        for (int i = 0; i < length; i++) {
            if(i == 0)
            {
                // perulangan j bawah ini berfungsi nyamain input di i, terus di cek sama gak
                for (int j = 0; j < length; j++) {
                    if (intArray[i] == intArray[j])
                    {
                        hitungSama ++;
                    }
                }
                arrayHitungSama[i] = hitungSama;
               // System.out.println(hitungSama);
                hitungSama = 0;
            }
            else
            {
                samaAtauTidak = false;
                // perulangan j bawah ini berfungsi untuk ngecek nilai j sekarang apakah sama dengan nilai i sebelumnya
                // outputnya boolean buat dipakai di if selanjutnya
                for (int j = 0; j < i; j++) {
                 if (intArray[i] == intArray[j])
                 {
                     samaAtauTidak = true;
                 }
                }
                // kalo j sekarang sama dengan i sebelum sebelumnya di masukkan ke array / tampungan dengan nilai 0
                // alasannya supaya gak ada dua angka sama ketika di print
                if (samaAtauTidak)
                {
                    arrayHitungSama[i] = 0;
                 //   System.out.println(hitungSama);
                }
                else
                {
                    for (int j = 0; j < length; j++) {
                        if (intArray[i] == intArray[j])
                        {
                            hitungSama++;
                        }
                    }
                    arrayHitungSama[i] = hitungSama;
                  //  System.out.println(hitungSama);
                    hitungSama = 0;
                }
            }
        }
        // perulangan di bawah digunakan untuk mencari arrayHitungsama yang paling banyak
       for (int i = 0; i < length; i++) {
           if (arrayHitungSama[i] >= hitungPalingBesar)
           {
               hitungPalingBesar = arrayHitungSama[i];
           }
       }
       // Setelah itu diulang kembali untuk di print
       for (int i = 0; i < length; i++) {
           if (arrayHitungSama[i] >= hitungPalingBesar)
           {
               hitungPalingBesar = arrayHitungSama[i];
               System.out.print(intArray[i] + " ");
           }
       }
   }
}