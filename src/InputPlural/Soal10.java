package InputPlural;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Mencari Bilangan Prima dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;
        int[] isiPrima = new int[length];

        for (int i = 0; i < length; i++) {
            hitung = 0;
            for (int j = 0; j < intArray[i]; j++) {
                if (intArray[i] % (j+1) == 0) {
                    hitung++;
                }
            }
            if (hitung == 2){
                isiPrima[i] = hitung;
            }
//            System.out.println(isiPrima[i]);
        }
        for (int i = 0; i < length; i++) {
            if(isiPrima[i] == 2){
                System.out.print(intArray[i] + ", ");
            }
        }
        System.out.println("Adalah bilangan prima");
    }
}

