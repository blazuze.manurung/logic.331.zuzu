package InputPlural;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Menghitung Banyaknya Angka yang Memiliki Pasangan");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;
        int[] arrayhitungKembar = new int[length];
        int jumlahKembar = 0;

        for (int i = 0; i < length; i++) {
                // perulangan j bawah ini berfungsi nyamain input di i, terus di cek sama gak
                for (int j = 0; j < length; j++) {
                    if (intArray[i] == intArray[j] && hitung < 2) {
                        hitung++;
                    }
                }
                arrayhitungKembar[i] = hitung;
               // System.out.print(arrayhitungKembar[i]);
                hitung = 0;
        }
        for (int i = 0; i < length; i++) {
            if (arrayhitungKembar[i] == 2)
            {
                jumlahKembar++;
            }
        }
        System.out.println(jumlahKembar / 2);
    }
}
