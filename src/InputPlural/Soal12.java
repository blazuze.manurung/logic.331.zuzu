package InputPlural;

import java.util.Arrays;
import java.util.Scanner;

public class Soal12 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Mencari Urutan Player Ketika Masuk Ke Leaderboard");
        System.out.println("Masukkan Data Leaderboard : ");
        String leaderboard = input.nextLine();

        System.out.println("Masukkan Data Player : ");
        String player = input.nextLine();

        int[] intLeader = Utility.ConvertStringToArrayInt(leaderboard);
        int[] intPlayer = Utility.ConvertStringToArrayInt(player);
        int[] intSemua = new int[intPlayer.length + intLeader.length];
        int hitung = 0;
        int[] urutan = new int[intSemua.length];
        int count = 0;

        for (int i = 0; i < intLeader.length; i++) {
            intSemua[i] = intLeader[i];
            hitung++;
        }

        for (int i = 0; i < intPlayer.length; i++) {
            intSemua[hitung] = intPlayer[i];
            hitung++;
        }

        for (int i = 0; i < intSemua.length; i++) {
            for (int j = 0; j < intSemua.length; j++) {
                if (intSemua[i] > intSemua[j])
                {
                    count ++;
                }
            }
            urutan[i] = count;
//            System.out.print(urutan[i]);
            count = 0;
        }

        System.out.println("Leaderboard Baru Ketika Data Player Dimasukkan : ");
        for (int i = (urutan.length - 1); i >= 0; i--) {
            for (int j = 0; j < urutan.length; j++) {
                if(i == urutan[j])
                {
                    System.out.print(intSemua[j] + " ");
                }
            }
        }
        System.out.println();
        int[] urutanRank = new int[intPlayer.length];
        System.out.println("Urutan Player :");
        for (int i = 0; i < intPlayer.length; i++) {
            for (int j = 0; j < urutan.length; j++) {
                if(intPlayer[i] == intSemua[j])
                {
                    urutanRank[i] = urutan[j] + 1;
                }
            }
        }

        Arrays.sort(urutanRank);

        for (int i = 0; i < urutanRank.length; i++) {
            System.out.print(urutanRank[i] + " ");
        }
    }
}
