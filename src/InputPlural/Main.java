package InputPlural;


import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        System.out.println("Pilih Nomor Soal (1-12)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 13)
        {
            System.out.println("Nomor Soal Tidak Tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1: Soal01.Resolve();
                break;
            case 2: Soal02.Resolve();
                break;
            case 3: Soal03.Resolve();
                break;
            case 4: Soal04.Resolve();
                break;
            case 5: Soal05.Resolve();
                break;
            case 6: Soal06.Resolve();
                break;
            case 7: Soal07.Resolve();
                break;
            case 8: Soal08.Resolve();
                break;
            case 9: Soal09.Resolve();
                break;
            case 10: Soal10.Resolve();
                break;
            case 11: Soal11.Resolve();
                break;
            case 12: Soal12.Resolve();
                break;
            case 13: Example.Resolve();
                break;
        }
    }
}