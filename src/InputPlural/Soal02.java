package InputPlural;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Mencari Rata - Rata dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();
        double jumlah = 0;
        double rata2 = 0;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
            jumlah += intArray[i];
            rata2 = jumlah / length;
        }
        System.out.println(rata2);
    }
}
