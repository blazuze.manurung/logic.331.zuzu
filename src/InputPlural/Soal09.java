package InputPlural;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Soal09 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Menghitung Persentase Angka Positive, Negative, dan Zero");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitungPositive = 0;
        int hitungNol = 0;
        int hitungNegative = 0;
        double persenPositive = 0;
        double persenNegative = 0;
        double persenNol = 0;

        for (int i = 0; i < length; i++) {
            if (intArray[i] == 0)
            {
                hitungNol++;
            }
            else if (intArray[i] > 0)
            {
                hitungPositive++;
            }
            else
            {
                hitungNegative++;
            }

        }
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        persenPositive = (double)100 * hitungPositive / length;
        persenNegative = (double)100 * hitungNegative / length;
        persenNol = (double)100 * hitungNol / length;

        System.out.println("Positive = " + decimalFormat.format(persenPositive) + " %");
        System.out.println("Negative = " + decimalFormat.format(persenNegative) + " %");
        System.out.println("Zero = " + decimalFormat.format(persenNol) + " %");
    }
}
