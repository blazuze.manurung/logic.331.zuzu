package InputPlural;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Mengurutkan Input Berupa Angka Tanpa Menggunakan Sort");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;
        int[] hitungUrutan = new int[length];

        // perulangan di bawah di gunakan untuk mencari nilai urutan
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (intArray[i] > intArray[j])
                {
                    hitung ++;
                }
            }
            hitungUrutan[i] = hitung;
           // System.out.println(hitungUrutan[i]);
            hitung = 0;
        }

        //perulangan di bawah digunakan untuk mengurutkan nilai urutan
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (i == hitungUrutan[j])
                {
                    System.out.print(intArray[j] + " ");
                }
            }
        }
    }
}
