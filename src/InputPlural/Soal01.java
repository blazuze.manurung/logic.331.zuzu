package InputPlural;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Menjumlahkan Semua Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int jumlah = 0;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
                jumlah += intArray[i];
        }
        System.out.println(jumlah);
    }
}
