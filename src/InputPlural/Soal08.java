package InputPlural;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Menghitung Banyaknya Angka Tertinggi Muncul dari Input");
        System.out.println("Tolong Masukkan Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;
        int palingBesar = 0;

        for (int i = 0; i < length; i++) {
            if (palingBesar < intArray[i])
            {
                palingBesar = intArray[i];
            }
        }

        for (int i = 0; i < length; i++) {
            if (intArray[i] == palingBesar)
            {
                hitung++;
            }
        }
        System.out.println(hitung);
    }
}
