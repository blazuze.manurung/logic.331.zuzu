package PreTest;

import java.util.Scanner;

public class Soal08 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Berapa Jumlah Data yang Diinginkan ; ");
        int n = input.nextInt();
        int hitung = 0;
        int jumlahData = 0;
        int counter = 0;
        int helper = 0;
        int helper1 = 1;

        int[] hasilPrima = new int[n];
        int[] hasilFibo = new int[n];
        int[] hasil = new int[n];

//      BILANGAN PRIMA
        for (int i = 0; i < n * 10; i++){
            for (int j = 1; j <= i; j++){
                if (i % j == 0)
                {
                    hitung++;
                }
            }
            if (hitung == 2){
                hasilPrima[counter] = i;
                counter++;
                jumlahData++;
            }
            if (jumlahData == n)
            {
                break;
            }
            hitung = 0;
        }
        System.out.print("Bilangan Prima = " );
        for (int i = 0; i < n; i++) {
            System.out.print(hasilPrima[i] + ", ");
        }
        System.out.println();

//      BILANGAN FIBONACCI
        for (int i = 0; i < n; i++)
        {
            if(i == 0)
            {
                hasilFibo[i] = helper1;
            }
            else
            {
                hasilFibo[i] = helper + helper1;
                helper = helper1;
                helper1 = hasilFibo[i];
            }
        }

        System.out.print("Bilangan Fibonacci = " );
        for (int i = 0; i < n; i++) {
            System.out.print(hasilFibo[i] + ", ");
        }
        System.out.println();

//      Total
        System.out.print("Output = " );
        for (int i = 0; i < n; i++) {
            hasil[i] = hasilFibo[i] + hasilPrima[i];
            System.out.print(hasil[i] + ", ");
        }
    }
}
