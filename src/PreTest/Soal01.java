package PreTest;

import java.util.Scanner;

public class Soal01 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Masukkan Nilai Maksimal : ");
        int jumlah = input.nextInt();

        int helperGanjil = 1;
        int helperGenap = 2;

        System.out.println("Ganjil : ");
        while (helperGanjil <= jumlah)
        {
           System.out.print(helperGanjil + " ");
           helperGanjil += 2;
        }
        System.out.println();

        System.out.println("Genap : ");
        while (helperGenap <= jumlah)
        {
            System.out.print(helperGenap + " ");
            helperGenap += 2;
        }
    }
}
