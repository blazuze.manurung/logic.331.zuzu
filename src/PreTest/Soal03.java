package PreTest;

import java.util.Scanner;

public class Soal03 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("====== Aku adalah Si Angka 1 =======");
        System.out.println("Masukkan banyaknya angka 1");
        System.out.print("Input = ");
        int inputN = input.nextInt();
        String awalString = "100";
        String[] output = new String[inputN];
        int hitungOutput = 0;
        boolean mulai = true;
        int counter = 0;

        while (mulai){
            int nilai = 0;
            int nilaiTotal = 0;
            boolean isSingle = false;
            String simpanSiAngka = awalString;
            while (!isSingle){
                for (int i = 0; i < awalString.length(); i++) {
                    nilai = Integer.parseInt(awalString.substring(i,i+1));
                    nilaiTotal += Math.pow(nilai,2);
                }
                awalString = String.valueOf(nilaiTotal);
                if (awalString.length() == 1 && nilaiTotal == 1){
                    isSingle = true;
                    output[hitungOutput] = simpanSiAngka;
                    hitungOutput ++;
                } else if (awalString.length() == 1 && nilaiTotal != 1) {
                    isSingle = true;
                }
                nilaiTotal = 0;
            }
            if (output.length == hitungOutput){
                mulai = false;
            }else {
                counter++;
                int awalInt = 100 + counter;
                awalString = Integer.toString(awalInt);
            }
        }

        System.out.println("OUTPUT");
        for (int i = 0; i < output.length; i++) {
            System.out.print(output[i]);
            System.out.println(" adalah \"Si Angka 1\"");
        }
    }
}


