package PreTest;

import java.util.Scanner;

public class Soal10 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.print("Saldo OPO = RP. ");
        int uang = input.nextInt();
        int helperUang = uang;
        int hargaKopi = 18000;
        int count = 0;
        int diskon = 0;
        int hargaKopiDiskon = 0;
        int bayar = 0;
        int cashback = 0;


        if(uang > 40000)
        {
            hargaKopiDiskon = hargaKopi * 50 / 100;
            while (helperUang > hargaKopiDiskon && diskon < 100000)
            {
                helperUang -= hargaKopiDiskon;
                count++;
                diskon = hargaKopiDiskon * count;
                if(diskon >= 100000)
                    diskon = 100000;
            }

            while (helperUang > hargaKopi)
            {
                helperUang -= hargaKopi;
                count++;
            }
            bayar = (count * hargaKopi) - diskon;
            cashback = bayar * 10 / 100;
            if (cashback >= 30000)
                cashback = 30000;
            uang = uang - bayar + cashback;
            System.out.println("Kopi = " + count);
            System.out.println("Sisa uang = RP. " + uang);
        }
        else
        {
            while (helperUang > hargaKopi)
            {
                helperUang -= hargaKopi;
                count++;
            }
            bayar = hargaKopi * count;
            cashback = bayar * 10 / 100;
            if (cashback >= 30000)
                cashback = 30000;
            uang = uang - bayar + cashback;
            System.out.println("Kopi = " + count);
            System.out.println("Sisa uang = RP. " + uang);
        }
    }
}
