package PreTest;

import java.util.Scanner;

public class Soal02 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        String vocal = "aeiou";
        String konsonan = "bcdfghjklmnpqrstvwxyz";

        char[] vocalCharArray = vocal.toCharArray();
        char[] konsonanCharArray = konsonan.toCharArray();

        System.out.println("Masukkan data : ");
        String data = input.nextLine();
        data = data.replaceAll(" ", "");
        char[] dataArray = data.toCharArray();


        System.out.print("Huruf Vocal = ");
        for (int i = 0; i < vocalCharArray.length; i++) {
            for (int j = 0; j < dataArray.length; j++) {
                if (vocalCharArray[i] == dataArray[j])
                {
                    System.out.print(dataArray[j] + " ");
                }
            }

        }
        System.out.println();
        System.out.print("Huruf Konsonan = ");
        for (int i = 0; i < konsonanCharArray.length; i++) {
            for (int j = 0; j < dataArray.length; j++) {
                if (konsonanCharArray[i] == dataArray[j]) {
                    System.out.print(dataArray[j] + " ");
                }
            }
        }
    }
}
