package PreTest;

import java.util.Scanner;

public class Soal07 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Masukkan Jumlah Kartu = ");
        int kartu = input.nextInt();
        System.out.println("Masukkan Tumlah Tawaran = ");
        int tawaran = input.nextInt();

        while (tawaran > kartu)
        {
            System.out.println("kartu lebih sedikit dari tawaran");
            System.out.println("Masukkan Tumlah Tawaran = ");
            tawaran = input.nextInt();
        }

        System.out.println("Pilihan Kotak A / B = ");
        String pilihan = input.next().toUpperCase();

        while (!pilihan.equals("A") && !pilihan.equals("B"))
        {
            System.out.println("pilihan tidak tersedia!");
            System.out.println("Pilihan Kotak A / B = ");
            pilihan = input.next().toUpperCase();
        }

        int kartuPemain = kartu;
        int kartuKomputer = kartu;

        int range = 10;
        int randomA = (int) (Math.random() * range);
        int isiKotakA = 0;
        isiKotakA += randomA;
        int randomB = (int) (Math.random() * range);
        int isiKotakB = 0;
        isiKotakB += randomB;

        if (pilihan.equals("A"))
        {
            System.out.println("A" + " || " + "B");
            System.out.println(isiKotakA + " || " + isiKotakB);
            if (isiKotakA > isiKotakB)
            {
                System.out.println("You Win !");
                kartuPemain += tawaran;
                kartuKomputer -= tawaran;
            }
            else
            {
                System.out.println("You Lose !");
                kartuPemain -= tawaran;
                kartuKomputer += tawaran;
            }
        }
        else if(pilihan.equals("B"))
        {
            System.out.println("A" + " || " + "B");
            System.out.println(isiKotakA + " || " + isiKotakB);
            if (isiKotakA > isiKotakB)
            {
                System.out.println("You Lose !");
                kartuPemain -= tawaran;
                kartuKomputer += tawaran;
            }
            else
            {
                System.out.println("You Win !");
                kartuPemain += tawaran;
                kartuKomputer -= tawaran;
            }
        }

        while(kartuPemain > 0 )
        {
            randomA = (int) (Math.random() * range);
            isiKotakA = 0;
            isiKotakA += randomA;
            randomB = (int) (Math.random() * range);
            isiKotakB = 0;
            isiKotakB += randomB;
            System.out.println("Masukkan Tumlah Tawaran = ");
            tawaran = input.nextInt();
            System.out.println("Pilihan Kotak A / B = ");
            pilihan = input.next().toUpperCase();

            while (tawaran > kartu)
            {
                System.out.println("kartu lebih sedikit dari tawaran");
                System.out.println("Masukkan Tumlah Tawaran = ");
                tawaran = input.nextInt();
            }

            if (pilihan.equals("A"))
            {
                System.out.println("A" + " || " + "B");
                System.out.println(isiKotakA + " || " + isiKotakB);
                if (isiKotakA > isiKotakB)
                {
                    System.out.println("You Win !");
                    kartuPemain += tawaran;
                    kartuKomputer -= tawaran;
                }
                else
                {
                    System.out.println("You Lose !");
                    kartuPemain -= tawaran;
                    kartuKomputer += tawaran;
                }
            }
            else if(pilihan.equals("B"))
            {
                System.out.println("A" + " || " + "B");
                System.out.println(isiKotakA + " || " + isiKotakB);
                if (isiKotakA > isiKotakB)
                {
                    System.out.println("You Lose !");
                    kartuPemain -= tawaran;
                    kartuKomputer += tawaran;
                }
                else
                {
                    System.out.println("You Win !");
                    kartuPemain += tawaran;
                    kartuKomputer -= tawaran;
                }
            }
        }
        if (kartuPemain == 0)
        {
            System.out.println("You Lose !!");
        } else if (kartuKomputer == 0)
        {
            System.out.println("You Win !!");
        }
    }
}
