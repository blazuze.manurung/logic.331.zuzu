package PreTest;

import java.util.Scanner;

public class Soal04 {
    public static Scanner input = new Scanner(System.in);
    public static void Resolve()
    {
        System.out.println("Contoh Input : Toko-Tempat 1-Tempat 2-Toko");
        System.out.println("Masukkan Rute : ");
        String data = input.nextLine().toLowerCase();

        String[] dataArray = data.split("-");

        int jarak = 0;

        for (int i = 1; i < dataArray.length; i++) {
            if(dataArray[1].contains("1"))
            {
                jarak += 2000;
            }
            else if(dataArray[1].contains("2"))
            {
                jarak += 2500;
            }
            else if(dataArray[1].contains("3"))
            {
                jarak += 4000;
            }
            else if(dataArray[1].contains("4"))
            {
                jarak += 6500;
            }
            else if (dataArray[i-1].contains("1") && dataArray[i].contains("1"))
            {
                jarak = jarak;
            }
            else if (dataArray[i-1].contains("1") && dataArray[i].contains("2"))
            {
                jarak += 500;
            } else if (dataArray[i-1].contains("1") && dataArray[i].contains("3"))
            {
                jarak += 1500;
            } else if (dataArray[i-1].contains("1") && dataArray[i].contains("4"))
            {
                jarak += 4500;
            } else if (dataArray[i-1].contains("2") && dataArray[i].contains("2"))
            {
                jarak = jarak;
            } else if (dataArray[i-1].contains("2") && dataArray[i].contains("1"))
            {
                jarak += 500;
            } else if (dataArray[i-1].contains("2") && dataArray[i].contains("3"))
            {
                jarak += 1500;
            } else if (dataArray[i-1].contains("2") && dataArray[i].contains("4"))
            {
                jarak += 4000;
            } else if (dataArray[i-1].contains("3") && dataArray[i].contains("3"))
            {
                jarak = jarak;
            } else if (dataArray[i-1].contains("3") && dataArray[i].contains("1"))
            {
                jarak += 2000;
            } else if (dataArray[i-1].contains("3") && dataArray[i].contains("2"))
            {
                jarak += 1500;
            } else if (dataArray[i-1].contains("3") && dataArray[i].contains("4"))
            {
                jarak += 2500;
            } else if (dataArray[i-1].contains("4") && dataArray[i].contains("4"))
            {
                jarak = jarak;
            } else if (dataArray[i-1].contains("4") && dataArray[i].contains("1"))
            {
                jarak += 4500;
            } else if (dataArray[i-1].contains("4") && dataArray[i].contains("2"))
            {
                jarak += 4000;
            }
            else if (dataArray[i-1].contains("4") && dataArray[i].contains("3"))
            {
                jarak += 2500;
            }
            else if (dataArray[i-1].contains("1") && dataArray[i].contains("o"))
            {
                jarak += 2000;
            }
            else if (dataArray[i-1].contains("2") && dataArray[i].contains("o"))
            {
                jarak += 2500;
            }
            else if (dataArray[i-1].contains("3") && dataArray[i].contains("o"))
            {
                jarak += 4000;
            }
            else if (dataArray[i-1].contains("4") && dataArray[i].contains("o"))
            {
                jarak += 6500;
            }
        }
        int bensin = jarak / 2500;
        System.out.println("Bensin yang digunakan : " + bensin + " liter");
    }
}
