package Array2D;

import java.util.Scanner;

public class Soal11{
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i+j >= (n-1))
                {
                    hasil[i][j] = 1;
                } else {
                    hasil[i][j] = 0;
                }
            }
        }
        Utility.PrintArray2DBintang(hasil);
    }
}
