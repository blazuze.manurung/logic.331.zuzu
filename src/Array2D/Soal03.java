package Array2D;

import java.util.Scanner;

public class Soal03 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[2][n];
        int urut = 0;
        int kaliDua = 3;
        boolean pembagi;
        if (n % 2 == 0)
        {
            pembagi = true;
        }
        else
        {
            pembagi = false;
        }

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    hasil[i][j] = urut;
                    urut++;
                } else {
                    if (pembagi)
                    {
                        if (j < (n / 2)-1) {
                            hasil[i][j] = kaliDua;
                            kaliDua *= 2;
                        }
                        else if (j == (n/2) - 1 ){
                            hasil[i][j] = kaliDua;
                        }
                        else
                        {
                            hasil[i][j] = kaliDua;
                            kaliDua /= 2;
                        }
                    }
                    else
                    {
                        if (j < (n / 2)) {
                            hasil[i][j] = kaliDua;
                            kaliDua *= 2;
                        }
                        else
                        {
                            hasil[i][j] = kaliDua;
                            kaliDua /= 2;
                        }
                    }
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
