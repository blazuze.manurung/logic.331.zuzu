package Array2D;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[2][n];
        int urut = 0;
        int kaliTiga = 1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    hasil[i][j] = urut;
                    urut ++;
                }
                else
                {
                    hasil[i][j] = kaliTiga;
                    kaliTiga *= 3;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
