package Array2D;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[3][n];
        int urut = 0;
        int bantuTambah1 = 0;
        int tambahDua = 0;
        int bantuTambah2 = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    hasil[i][j] = urut;
                    urut++ ;
                }
                else if (i == 1)
                {
                    hasil[i][j] = tambahDua;
                    tambahDua += 2;
                }
                else
                {
                    hasil[i][j] = bantuTambah1 + bantuTambah2;
                    bantuTambah1 ++;
                    bantuTambah2 += 2;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
