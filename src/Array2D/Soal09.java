package Array2D;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[3][n];
        int urut = 0;
        int tambahTiga = 0;
        int bantuBagi = (n-1) * 3;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    hasil[i][j] = urut;
                    urut++ ;
                }
                else if (i == 1)
                {
                    hasil[i][j] = tambahTiga;
                    tambahTiga += 3;
                }
                else
                {
                    hasil[i][j] = bantuBagi;
                    bantuBagi -= 3;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
