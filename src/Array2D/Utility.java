package Array2D;

public class Utility {
    public static void PrintArray2D(int[][] hasil)
    {
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                System.out.print(hasil[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void PrintArray2DBintang(int[][] hasil)
    {
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil.length; j++) {
                if (hasil [i][j] == 0)
                {
                    System.out.print("  ");
                }
                else
                {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    }
}
