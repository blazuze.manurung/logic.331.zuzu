package Array2D;

import java.util.Scanner;

public class Example {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[2][n];
        int ganjil = 1;
        int genap = 2;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    hasil[i][j] = ganjil;
                    ganjil += 2;
                }
                else
                {
                    hasil[i][j] = genap;
                    genap += 2;
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
