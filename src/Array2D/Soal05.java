package Array2D;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[3][n];
        int urut = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                hasil[i][j] = urut;
                urut++;
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
