package Array2D;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[3][n];
        int urut = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    if (j % 3 == 0) {
                        urut *= -1;
                        hasil[i][j] = urut;
                        urut *= -1;
                        urut++;
                    } else {
                        hasil[i][j] = urut;
                        urut++;
                    }
                } else if (i == 1) {
                    if (urut % 3 == 0) {
                        urut *= -1;
                        hasil[i][j] = urut;
                        urut *= -1;
                        urut++;
                    } else {
                        hasil[i][j] = urut;
                        urut++;
                    }
                } else if (i == 2) {
                    if (urut % 3 == 0) {
                        urut *= -1;
                        hasil[i][j] = urut;
                        urut *= -1;
                        urut++;
                    } else {
                        hasil[i][j] = urut;
                        urut++;
                    }
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
