package Array2D;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve ()
    {
        Scanner input = new Scanner(System.in);

        int n = 0;

        System.out.println("Ketikkan Jumlah Angka yang Diinginkan :");
        n = input.nextInt();

        int[][] hasil = new int[2][n];
        int urut = 0;
        int antara = 1;
        int ambil = 1;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0)
                {
                    hasil[i][j] = urut;
                    urut ++;
                }

                else
                {
                    if ((j + 1) % 2 == 0) {
                        antara *= 5;
                        hasil[i][j] = antara;
                        antara = ambil;
                    } else {
                        hasil[i][j] = ambil;
                        ambil++;
                    }
                }
            }
        }
        Utility.PrintArray2D(hasil);
    }
}
